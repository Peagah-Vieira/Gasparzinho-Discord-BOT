**<p align="center"> <img src="https://cdn.discordapp.com/icons/1029848897167835177/cf7be122279d1fd2432ee255a2f82859.png?size=4096" /> </p>**


<h1 align="center">GASPARZINHO-1.0</h1>
<p align="center">
<a href="https://github.com/Peagah-Vieira/Gasparzinho-Discord-BOT/blob/master/LICENSE"><img alt="GitHub License" src="https://img.shields.io/github/license/Peagah-Vieira/Gasparzinho-Discord-BOT?style=for-the-badge"></a>
<a href="https://github.com/Peagah-Vieira/Gasparzinho-Discord-BOT"><img alt="GitHub Stars" src="https://img.shields.io/github/stars/Peagah-Vieira/Gasparzinho-Discord-BOT?style=for-the-badge"></a> 
<a href="https://github.com/Peagah-Vieira/Gasparzinho-Discord-BOT/network"><img alt="GitHub Forks" src="https://img.shields.io/github/forks/Peagah-Vieira/Gasparzinho-Discord-BOT?style=for-the-badge"></a>
<h3 align="center">An Multi-purpose Discord bot with many features!</h3>

---

## Requirements
- Discord.js v14 (`npm install discord.js@latest`)
- `applications.commands` scope enabled for your bot in Developer Portal (For Slash Cmds).
- `MESSAGE_CONTENT`, `GUILD_MEMBERS` intents enabled in Developer Portal (`GUILD_PRESENCES` is optional)
- NodeJS v16.9.0 or higher
- Basic knowledge of JS or Discord.JS

## Have suggestions on what could be added?
- **Leave your suggestions right [here](https://github.com/Peagah-Vieira/Gasparzinho-Discord-BOT/discussions/1) then!**

## What features does Gasparzinho Include?

<details><summary>Available Features</summary>

| Features             | Availability |
| -------------------- | ------------ |
| User Info            |     ✅       |
| Server Info          |     ✅       |
| Music Commands       |     ✅       |
| Welcome Message      |     ✅       |
| Leave Message        |     ✅       |
| Auto Role            |     ✅       |
| Role ADD/REMOVE      |     ✅       |

</details>

## Versions and Support Info

<details><summary>Detailed Versions Info</summary>

|              Gasparzinho Versions                      | Support Status |
| ------------------------------------------------------ | -------------- |
| v1.5.0-alpha (Strutural Changes)                       |       Available          |
| v1.0.0-alpha (Inicial Features)                        |       discontinued       |

</details>

- **Keep checking the [Releases Section](https://github.com/Peagah-Vieira/Gasparzinho-Discord-BOT/releases) to get the latest info relating new updates, bug fixes etc about the repository.**
- **The Information above includes Versions with only `Major Updates` and not Versions with `Bug fixes`**

---

## Getting started
#### Creating a fork:
- 1). [Click here to fork the repository](https://github.com/Peagah-Vieira/Gasparzinho-Discord-BOT)
- 2). Open your terminal and type `git clone https://github.com/Peagah-Vieira/Gasparzinho-Discord-BOT.git`
#### Installing all necessary packages
- `npm install`
#### Starting the bot
- `node .` or `node index` 

---

## Configuration
- **Edit the `env` file and enter the  required values**
```env
# Bot Token
BOT_TOKEN=""

# Guild ID
GUILD_ID=""

# User/Bot ID
OWNER_ID=""
DISBOARD_ID=""
MEE6_ID=""
INVTRACK_ID=""

# Bot Emojis
BOT_CONF=""
BOT_DENY=""
BOT_INFO=""
BOT_DOC=""
LIVE_NOW=""

# Channels
BOT_CHAN=""
RULE_CHAN=""
PREM_CHAN=""
STAFF_CHAN=""
FAQ_CHAN=""
RES_CHAN=""
CKQ_CHAN=""
BUMP_CHAN=""
INSIDER_CHAN=""
INVITE_CHAN=""
TWITCH_PROMO=""
BOOSTER_PROMO=""
GENERAL_CHAN=""
WELCOME_CHAN=""
CCREW_CHAN=""
SELFROLE_CHAN=""
LL_CHAN=""
COUNT_CHAN=""
TEST_CHAN=""

# Log Channels
USERUP_CHAN=""
BAN_CHAN=""
JOINLEAVE_CHAN=""
MUTES_CHAN=""
STAFF_APP2=""
CMDLOG_CHAN=""
WARN_CHAN=""
MSGUP_CHAN=""
MSGDEL_CHAN=""
BL_CHAN=""
MCHOICE_LOG_CHAN=""

# Voice Channels
VC_HUB=""
VC_CATEGORY=""
VC_ONLINE=""
VC_TOTAL=""

# Roles
BOT_ROLE=""
STAFF_ROLE=""
MOD_ROLE=""
RANK5_ROLE=""
RANK10_ROLE=""
RANK15_ROLE=""
RANK20_ROLE=""
RANK25_ROLE=""
RANK30_ROLE=""
CKQ_ROLE=""
BUMP_ROLE=""
VERIFIED_ROLE=""
SEASON_ROLE=""
BOOST_ROLE=""
LIVE_ROLE=""
CCREW_ROLE=""
TEST_ROLE=""

# Database
DB_PATH=""

# Self Roles Message IDs
SELF_ROLES_PLATFORM_ID=""
SELF_ROLES_NICKNAME_COLOR_ID=""
SELF_ROLES_AGE_ID=""
SELF_ROLES_REGION_ID=""
SELF_ROLES_GENDER_ID=""
SELF_ROLES_CUSTOM_ID=""
```
---

## Contributing
- **Join our [Community server](https://discord.gg/6wwhYF4TB3)**

---

## Star the Repo if you liked it!
